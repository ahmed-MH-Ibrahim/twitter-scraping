import $ from "jquery";
import swal from 'sweetalert2';
import  '../assets/css/content.css';
import { createApp } from 'vue';
import TwitterParser from '../view/parser.vue';

//global vars
var isPageLoaded = false;
var twitterParser;
var url;

$(document).ready(function(){

	//appending stylesheets
	$('body').append('<link rel="stylesheet" type="text/css" href="'+chrome.runtime.getURL('css/content.css')+'">');

	//append additional views
	$("body").append("<div id='twitter-scraping-ui-container' class='twitter-scraping-ui-container'></div>");
	
	//append translate popup vue to site dom
	twitterParser = createApp(TwitterParser).mount('#twitter-scraping-ui-container');

	//handle extension events
	chrome.runtime.onMessage.addListener(function(request, sender, sendResponse)
	{
	    if(request.command == "is-page-loaded")
	    {
	      //get url every check in case user redirects without page refresh
	      url = window.location.href;
	      isPageLoaded = true;

	      //Update popup with current page type and page loaded state
	      chrome.runtime.sendMessage({"command": "page-loaded",isPageLoaded:isPageLoaded,parsingPostsStarted:twitterParser.parsingPostsStarted});                  
	    }
	    else if(request.command == "start-scan")
	    {	      
	      //add window to dom to show script starting
	      twitterParser.startWindowDom();

	      twitterParser.startScan();

	    }
	    else if(request.command == "cancel-scan")
	    {
	      twitterParser.cancelScan();
	    }
	});
});